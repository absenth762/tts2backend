#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""@file   setup.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   16 Dec 2022

@brief tts2backend is the backend for trythissoap.com


Copyright © 2022 Dj Djundjila, TTS Rebuild Committee

tts2backend is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

tts2backend is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""


from setuptools import setup, find_packages

setup(
    name="tts2backend",
    packages=find_packages(),
    author="Dj Djundjila",
    author_email="djundjila.gitlab@cloudmail.altermail.ch",
    test_suite="tests",
)
